// Written by danvogel@thedccurrent.com
// ==UserScript==
// @name          Shopify Inventory Spy
// @namespace     http://confluenceultimate.com/grease
// @downloadURL  http://confluenceultimate.com/grease/shopify_inventory_show.user.js
// @description  Reveal available inventory on a shopify site
// @include       http://shop.mlultimate.com/products/*
// @match         http://*/products/*
// @run-at        document-end
// @version       0.2
// @grant         none
// ==/UserScript==

(function () {

  var
    styleForDiv = {
      'position': 'fixed',
      'top': '0px',
      'left': '0px',
      'background-color': '#afafaf',
      'color': '#000',
      'padding': '0em',
      'margin': '0em',
      'z-index': '20'
    },

    styleForUL = {
      'list-style-type': 'none',
      'text-align': 'left',
      'padding': '0em',
      'margin': '0em'
    },

    dumpFn = function (data) {
      var
        // create a receptical to present output
	outputBox = (function () {
          var
            // create a styled DIV at the top left corner
	    box = (function () {
              var floating_box = $('<div id="shopify-inv-show"></div>');
              floating_box.appendTo('body');
              floating_box.html('');
              floating_box.css(styleForDiv);
              return floating_box;
	    }()),
            // put a styled unsorted list in the box to append to.
	    list = $("<ul></ul>").css(styleForUL);
	    box.append(list);
	    // return the list within the box to append items to
	    return list;
	}()),

        // function for outputing the sku and quantity details 
        output_variant = function (sku, quantity) {
          var
            output_txt = sku + ":  " + quantity,
            output_li = $("<li></li>").text(output_txt);

          // write to the javascript console if available
          if (window.console && console && console.info) {
            console.info(output_txt);
          }
          // add it to our special visible box
          outputBox.append(output_li);
          return;
        };

        if (!data) {
          console.warn("No Shopify product data retrieved");
          return;
        }

        if (!data.variants) {
          console.warn("No Shopify product variants retrieved");
          return;
        }

        $.each(data.variants, function (idx, e) {
          // prefer the SKU if available
          if (this.sku) {
            return output_variant(this.sku, this.inventory_quantity);
          }
          if (this.title) {
            return output_variant(this.title, this.inventory_quantity);
          }
          console.warn('odd variant', this);
        });

        return;
      },

      current_url = $(location).attr('href');

    // should be handled by greasemonkey
    if (current_url.match('/products/')) {
      ajax_req = $.getJSON(current_url + ".js", dumpFn);
      console.log("Checking for product inventory");
    }
}());